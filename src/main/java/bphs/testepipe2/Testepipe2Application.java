package bphs.testepipe2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Testepipe2Application {

	public static void main(String[] args) {
		SpringApplication.run(Testepipe2Application.class, args);
	}

}
